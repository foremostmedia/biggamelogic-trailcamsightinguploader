﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Text.RegularExpressions;

namespace TrailCamImageDateTimeGenerator
{
    class Program
    {
        static void Main(string[] args)
        {
            var files = Directory.GetFiles(AppDomain.CurrentDomain.BaseDirectory, "*.jpg");
            var dates = files.Select(GetDateTakenFromImage).ToList();

            File.WriteAllLines(string.Format("{0}/TrailCamImageDateTimes-{1}.txt", AppDomain.CurrentDomain.BaseDirectory,DateTime.Now.ToString("yyyyMMdd")), dates.Where(x => x != DateTime.MinValue).Select(x => x.ToString()));
        }

        //we init this once so that if the function is repeatedly called
        //it isn't stressing the garbage man
        private static Regex r = new Regex(":");

        //retrieves the datetime WITHOUT loading the whole image
        public static DateTime GetDateTakenFromImage(string path)
        {
            try
            {
                using (var fs = new FileStream(path, FileMode.Open, FileAccess.Read))
                {
                    using (Image myImage = Image.FromStream(fs, false, false))
                    {
                        PropertyItem propItem = myImage.GetPropertyItem(36867);
                        string dateTaken = r.Replace(Encoding.UTF8.GetString(propItem.Value), "-", 2);
                        return DateTime.Parse(dateTaken);
                    }
                }
            }
            catch(Exception ex)
            {
                return DateTime.MinValue;
            }
        }
    }
}
